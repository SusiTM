\chapter{Text Categorization}

\section{Introduzione}
Uno dei temi più comuni del \textit{Text Mining} è la \textit{Text Categorization}: dato un insieme di categori (argomenti, topics) ed una collezione di documenti, si richiede di trovare il topic corretto per ogni documento.

Lo studio della \textit{Automated Text Categorization} inizia negli anni '60, ma è solo negli anni '90 che si assiste ad un sostanziale sviluppo dovuto anche al crescente numero di documenti in forma digitale.

Oggi la \textit{Automatic Text Categorization} ha svariati utilizzi: text indexing, document sorting, spam filtering, categorizzazione delle pagine web in cataloghi gerarchici...

Esistono due approcci alla \textit{Text Categorization}:
\begin{itemize}
	\item \textit{Knowledge Engineering approach}: la conoscenza degli esperti del dominio viene codificata attraverso regole.
	\item \textit{Machine Learning approach}: tramite un processo induttivo viene generato un classificatore da un insieme di esempi pre-classificati.
\end{itemize}

L'approccio che verrà analizzato è quello basato sull'apprendimento automatico.
\section{Definizione del problema}
In generale, la \textit{Text Categorization} ha come obiettivo quello di approssimare una funzione sconosciuta di assegnamento a categorie\( F : D \times C \rightarrow \{ 0, 1\} \) , dove \(D\) è l'insieme dei documenti, mentre \(C\) è l'insieme delle categorie. $F$ è definita come:

\[
F(d,c) = \left\{ 
\begin{array}{l l}
  1 & \quad \textrm{se il documento $d$ appartiene alla categoria $c$} \\
  0 & \quad \textrm{altrimenti} \\
\end{array} \right.
\]

La funzione di approssimazione \( M : D \times C \rightarrow \{ 0, 1\} \) è chiamata classificatore. Il problema è generare un classificatore che produca risultati il più possibile vicini alla reale funzione di classificazione $F$.

\subsection{Single/Multi Label Categorization}
A seconda delle proprietà di $F$ possiamo distinguere due diverse tipologie di classificazione.
\begin{itemize}
	\item \textit{Multi-Label}: le categorie si sovrappongono, quindi un documento può appartenere a più categorie.
	\item \textit{Single-Label}: ogni documento appartiene esattamente ad una categoria.
\end{itemize}

La classificazione binaria è un caso speciale della classificazione single-label dove il numero di categorie è due. Si tratta della classificazione più utilizzata. Inoltre, il caso \textit{multi-label} può essere risolto da $|C|$ classificatori binari.

\subsection{Document/Category pivoted}
Solitamente, i classificatori sono utilizzati nel seguente modo: dato un documento, il classificatore deve trovare tutte le categorie a cui quel documento appartiene. Questa è chiamata categorizzazione \textit{document-pivoted}. Dualmente, potremmo aver bisogno di trovare tutti i documenti che appartengano ad una categoria fissata. Questa è chiamata categorizzazione \textit{category-pivoted}.
La differenza tra i due è rilevante, specialmente quando la categorizzazione è \textit{on-line}, cioè non sono sempre disponibili tutti i documenti o tutte le categorie.

\subsection{Hard/soft categorization}
Un sistema di categorizzazione completamente automatizzato deve prendere una decisione binaria per ogni coppia documento-categoria. Questi sistemi eseguono una \textit{hard categorization}. Le performance raggiunte da questi sistemi possono non essere completamente soddisfacenti. In questi casi, un approccio semi-automatizzato è preferibile. In questi sistemi è un umano che prende la decisione finale riguardo le categorie da assegnare ad un documento, mentre il sistema presenta una lista di categorie ordinata secondo la stima di appartenenza. In questo caso il sistema esegue una \textit{Soft (ranking) Categorization}. Molti classificatori producono per ogni coppia documento-categoria un valore reale compreso tra 0 e 1, chiamato \textit{Categorization Status Value}. Questi classificatori eseguono quindi naturalemtne un ranking. Decisioni binarie possono essere prese sulla base di un valore soglia, che può essere calcolato automaticamente (possibilmente massimizzando le performance del classificatore su un validation set).

\section{Rappresentazione dei documenti}
Gli algoritmi di apprendimento e classificazione non possono lavorare direttamente sui file di testo nella loro forma originale. Per questo è necessario effettuare un passaggio di \textit{pre-processing}, in cui i documenti sono convertiti in una forma più maneggevole. Tipicamente, i documenti vengono trasformati in \textit{feature vectors}. Gli approcci più comuni utilizzano come feature semplicemente tutte le parole nei documenti. Sono presenti vari metodi per assegnare pesi alle feature. Il più comune è lo schema TF-IDF, che data una parola $w$ nel documento $d$ assegna il peso nel seguente modo:
\[
\textit{TF-IDF}(w,d) = TermFreq(w,d) \cdot log (N \slash DocFreq(w))
\]

dove $TermFreq(w,d)$ è la frequenza della parola $w$ nel documento $d$, $N$ è il numero di documenti e $DocFreq(w)$ è il numero di documenti che contengono il termine $w$.

\subsection{Feature selection}
Molte delle parole presenti nei documenti sono irrilevanti ai fini della categorizzazione e possono essere scartate senza influenzare le performance del classificatore. Il passo del text preprocessing che rimuove le parole irrilevanti è chiamato \textit{feature selection}.
Una tecnica molto semplice consiste nell'usare \textit{stop words}, cioè liste precompilate di parole irrilevanti.
Altre tecniche più avanzate analizzano la rilevanza di ogni feature. Una tecnica semplice analizza la \( DocFreq(w)\). Esperimenti dimostrano che mantenendo solo il primo 10 per cento delle parole più frequenti non si riducono le performance del classificatore.
Esistono anche altre tecniche più complesse, ad esempio l'\textit{Information Gain}:
\[
IG(w)=\sum_{c \in C \cup \bar{C}} \sum_{f \in \{w,\bar{w}\}} P(f, c) \cdot log \frac{P (c | f)}{P(c)}
\]
 Le probabilità sono stimate sulla base della frequenza nel training set.
Un'altra buona misura è il \textit{chi-squared}:

\[
\chi_{max}^2= \max_{c \in C} \frac{|Tr|\cdot(P(f,c) \cdot P(\bar{f},\bar{c}) - P(f,\bar{c}) \cdot P(\bar{f},c))^2}{P(f) \cdot P(\bar{f}) \cdot P(c) \cdot P(\bar{c})}
\]

Esperimenti dimostrano che entrambe queste misure possono ridurre la dimensionalità delle feature di un fattore 100 senza perdita di qualità di categorizzazione.

\subsection{Feature extraction}
Un altro modo per ridurre lo spazio delle features è quello di creare un nuovo insieme di feature, non necessariamente con termini presenti nei documenti. Corrisponde a creare una trasformazione dallo spazio delle feature originale ad un altro spazio a molte meno dimensioni. Questo metodo combatte i problemi legati alla polisemia, omonimia e sinonimia.
Una tecnica è quella del \textit{term clustering}, cioè unire in una sola feature vari termini semanticamente fortemente correlati. Questi gruppi di parole sono poi utilizzati come feature anzichè le singole parole.
Un altro approccio più complesso è quello del \textit{Latent Semantic Indexing}.

\section{Approccio Knowledge engineering}
Questo approccio alla \textit{Text Categorization} si focalizza sulla creazione manuale di regole per la classificazione, che devono essere definite da un esperto del dominio.
Anche se le performance raggiungibili da questo tipo di sistemi sono più alte di quelle ottenibili dall'approccio di tipo \textit{machine learning}, non viene utilizzato poichè potrebbero essere necessari anni per definire un insieme di regole soddisfacente. Questo problema rende l'altro approccio molto più utilizzato.

\section{Approccio machine learning}
Nell'approccio ML, il classificatore è costruito automaticamente a partire da un insieme di \textit{training} costituito da documenti pre-classificati.
Si tratta di un istanza di apprendimento \textit{supervisionato} poiché si conosce il vero valore della funzione di assegnamento nel training set. La versione non supervisionata della classificazione è chiamata {clustering}, e verrà trattata nel prossimo capitolo.
Esistono vari approcci per apprendere il classificatore, alcuni dei quali sono varianti di algoritmi più generali di ML, mentre altri sono stati creati specificatamente per la categorizzazione.

Un approccio di tipo \textit{Machine Learning} è costituito da quattro ingredienti principali:
\begin{itemize}
	\item Le categorie da utilizzare per classificare le istanze.
	\item Il \textit{training set}: indicativamente sono necessari 30 esempi per ogni categoria.
	\item Lo spazio delle \textit{features} da utilizzare per rappresentare i documenti.
	\item L'algoritmo da utilizzare per la categorizzaizone.
\end{itemize}

\subsection{Classificatori probabilistici}
I classificatori probabilistici vedono la \textit{Categorization Status Value} $CSV(d,c)$ come la probabilità $P(c | d)$ che il documento d appartenga alla categoria c, e calcolano questa probabilità applicando il teorema di Bayes:
\[
P(c|d) = \frac{P(d|c)P(c)}{P(d)}
\]
La probabilità a priori P(d) non deve essere calcolata perchè è costante per tutte le categorie.
Per stimare $P(c|d)$ è necessario fare delle assunzioni riguardo la struttura del documento d. Assumiamo che i documenti siano rappresentati come vettori di \textit{feature} \( d=(w1,w2, \ldots)\). L'assunzione più comune è quella di indipendenza dei termini dei classificatori \textit{Na\"ive Bayes}:
\[
P(d|c)= \prod_{i} P(w_{i}|c)
\]
Questi classificatori vengono chiamati \textit{Na\"ive} poichè questa assunzione è palesemente falsa. Questi classificatori si comportano tuttavia abbastanza bene, e modelli più complessi che tentano di rilassare questa assunzione non hanno prodotto consistenti miglioramenti nelle performance.

\subsection{Bayesian Logistic Regression}

\'E possibile anche stimare direttamente la probabilità $P(c|d)$. La \textit{Bayesian Logistic Regression} è un approccio statistico al problema. Assumendo la categorizzazione binaria, il modello diventa nella forma:

\[
P(c|d)=\varphi(\beta \cdot d) = \varphi(\sum_{i}\beta_{i} d_{i})
\]

dove c = $\pm 1$ è il valore della funzione di membership, $d=(d_{1}, d_{2}, \ldots)$ è la rappresentazione dei documenti nel feature space, $\beta$ sono i parametri del modello (da apprendere) e $\varphi$ è la \textit{logistic link function}

\[
\varphi(x) = \frac{e^x}{1+e^x}= \frac{1}{1+e^{-x}}
\]

In questo modello è importante evitare il fenomeno di \textit{overfitting}. Si possono usare varie distribuzioni di probabilità per il vettore $\beta$, ad esempio è possibile utilizzare una distribuzione di probabilità gaussiana con media 0 e varianza $\tau$:

\[
p(\beta_{i} | \tau )= \frac{1}{\sqrt{2 \pi \tau}} \exp(\frac{-\beta_{i}^2}{2 \tau})
\]

In alternativa è possibile utilizzare la distribuzione di Laplace, per permettere ad alcune probabilità di assumere valore zero e quindi ammettere la sparsità del modello.

La probabilità a posteriori $\beta$, una volta stimata, verrà utilizzata per le previsioni. Per effetturare questa stima, solitamente si sceglie l'ipotesi \textit{Maximum a posteriori}, cioè:

\[
H_{MAP}= \arg\max_{\beta} l(\beta)
\]
con $l(\beta)$ definita come:
\[
l(\beta) = p(\beta | D) = - \left(\sum_{(d,c) \in D} ln( exp ( -c \beta \cdot d) +1) \right) + \ln p(\beta)
\]
\[
\ln p(\beta) = - \left( \sum_{i} \left( \ln \sqrt(\tau) + \frac{\ln 2 \pi}{2} + \frac{\beta_{i}^2}{\tau} \right) \right)
\]

 per la distribuzione gaussiana vista in precedenza.


\subsection{Alberi di decisione}
\begin{figure}[ht]
\begin{center}
\includegraphics[width=120mm]{DT_tabella.png}
\caption{Tabella esempio di Training set}
\label{DT_table}
\end{center}
\end{figure}

\begin{figure}[ht]
\begin{center}
\includegraphics[width=120mm]{DT_albero.png}
\caption{Esempio di albero di decisione}
\label{DT_tree}
\end{center}
\end{figure}
Molti classificatori hanno il problema  che non possono essere facilmente compresi dagli esseri umani. I classificatori simbolici, di cui gli alberi di decisione sono l'esempio più diffuso, non sono affetti da questo problema.
Un \textit{albero di decisione} è un albero in cui i nodi interni sono etichettati dalle \textit{features}, gli archi uscenti dai nodi sono etichettati con test sul valore della \textit{feature}, e le foglie sono etichettate con le categorie. Un esempio di albero di decisione, costruito a partire dai dati in tabella \ref{DT_table} è riportato in figura \ref{DT_tree}. Un albero di decisione categorizza un documento partendo dalla radice dell'albero e muovendosi verso le foglie seguendo i rami in cui le condizioni sono soddisfatte dal documento, finchè non viene raggiunto un nodo foglia. Al documento viene poi assegnata la categoria indicata da quella foglia. La maggioranza dei DT usa una rappresentazione binaria dei documenti, di conseguenza gli alberi di decisione sono alberi binari.





I principali algoritmi utilizzati per la costruzione degli alberi di decisione sono \textit{ID3} \textit{C4.5} e \textit{CART}.
Tipicamente, l'albero viene costruito in maniera ricorsiva, scegliendo una feature $f$ ad ogni passo e dividendo il training set in due sottoinsiemi, uno contenente la feature $f$ e uno che non la contiene, finchè non rimangono documenti di una sola categoria nell'insieme. A questo punto viene generato un nodo foglia.

La scelta della feature è effettuata sulla base di alcune msure di teoria dell'informazione (information gain o entropia). Gli alberi generati in questo modo tendono all'overfitting, quindi vengono utilizzate tecniche di pruning (rimuovere i rami dell'albero troppo specifici).

Le performance degli alberi di decisione non sono delle migliori, per questo vengono raramente utilizzati in contesti dove la comprensione umana non è essenziale. Sono spesso utilizzati nei comitati di classificatori, che vedremo in seguito.

\subsection{Regole di decisione}
I classificatori basati su regole appartengono alla famiglia dei classificatori simbolici. Le regole sono in \textit{Disjunctive Normal Form}. Sono costruite con metodi \textit{bottom-up}. Il classificatore iniziale (più specifico) è costruito direttamente dagli esempi nel training set, considerando ogni esempio come una clausola:
\[
d_{1} \wedge d_{2} \wedge \ldots \wedge d_{n} \rightarrow c
\]


 Il classificatore poi applica una serie di generalizzazioni, rimuovendo termini dalle clausole e unendo regole.
Alla fine del processo viene effettuato il \textit{pruning} similmente agli alberi di decisione, per rendere il classificatore più generale.

\subsection{Regressione}
\begin{figure}[ht]
\begin{center}
\includegraphics[width=120mm]{Linear_regression.png}
\caption{Esempio di regressione lineare.}
\label{Regression}
\end{center}
\end{figure}

La regressione è una tecnica per approssimare una funzione a valori reali conoscendo solo i suoi valori in alcuni punti. Questa tecnica può essere applicata alla \textit{Text Categorization} approssimando la funzione di assegnamento delle categorie ai documenti.
Un metodo (lineare) che si può utilizzare è il metodo dei minimi quadrati. In questo metodo la funzione di assegnamento alle categorie è vista come una matrice di dimensioni $|C| \mathrm{x} |F|$, che descrive una trasformazione lineare dallo spazio delle \textit{features} a quello di tutti i possibili assegnamenti di categorie. Per costruire il classificatore, cerchiamo la matrice che si adatti meglio ai dati di training. In dettaglio, minimizziamo l'errore nel \textit{training set} secondo la formula:
\[
M = \arg \min_{M} \| MD - O\|_{F}
\]

dove $D$ è la matrice delle rappresentazioni degli esempi, di dimensione $|F| \mathrm{x} |TrainingSet|$, $O$ è la matrice che rappresenta, per ogni documento, le categorie a cui appartiene di dimensione $|C| \mathrm{x} |TrainingSet|$, mentre $\| \cdot \|_{F}$ sta ad indicare la norma di Frobenius, definita come:

\[
\|A\|_{F} = \sqrt{\sum A_{ij}^{2}}
\]


\subsection{Metodo di Rocchio}
 Il classificatore di Rocchio classifica i documenti calcolando la distanza dal vettore \textit{profilo} della categoria.
Il vettore profilo per una certa categoria $c_{i}$ è un vettore $(w_1, w_2, \ldots)$ calcolato secondo la formula:
\[
 w_i = \frac{\alpha}{POS(c)} \sum_{d \in POS(c)} w_{di} - \frac{\beta}{NEG(c)} \sum_{d \in NEG(c)} w_{di}
\] 

dove $POS(c)$ e $NEG(c)$ sono gli insiemi dei documenti di training che rispettivamente appartengono o non appartengono alla categoria $c$, e $w_{di}$ è il peso dell'i-esima feature nel documento $d$. Solitamente gli esempi positivi sono considerati più importanti, quindi $\alpha >> \beta$.
Il metodo di Rocchio è molto facile da implementare e computazionalmente efficiente, ma ha performance mediocri. Tuttavia esistono alcune variazioni di questo metodo che usano vari accorgimenti, che sono considerate lo stato dell'arte.

\subsection{Classificatori Example-based}

Questo tipo di classificatori calcolano le differenze tra il documento da classificare e gli esempi a disposizione. Questi metodi sono chiamati anche \textit{lazy learners}  poichè eseguono lavoro solo quando devono classificare un nuovo esempio. La fase di \textit{training} infatti è costituita semplicemente dal rappresentare (e tenere in memoria) i documenti del \textit{training set}, insieme alle rispettive \textit{label}.

Il più importante esempio di questo tipo di classificatori è K-Nearest-Neighbour. 

\subsubsection{KNN}
Per decidere a quale categoria appartiene un documento $d$, kNN controlla a quale categoria appartengono i k documenti del \textit{training set} più simili a $d$, ed assegna la categoria di maggioranza. \'E cruciale la scelta del numero $k$, il cui valore può essere appreso attraverso un \textit{validation set}, oppure può essere scelto un valore a priori.
KNN è tra gli algoritmi di classificazione più performanti, perchè non assume che gli esempi siano linearmente separabili. Il problema è l'elevato costo computazionale della classificazione, infatti deve essere calcolata la similarità con ogni esempio del \textit{training set} ogni volta che è necessario classificare un documento.

\subsection{Support Vector Machines}
L'algoritmo SVM è molto veloce e funziona molto bene nell'ambito della \textit{Text Classification}. In termini geometrici, una SVM può essere vista come un iperpiano nel cosiddetto \textit{feature space} che separa gli esempi positivi da quelli negativi. L'iperpiano separatore che viene scelto è unico ed è quello che massimizza il margine, cioè la distanza tra l'iperpiano ed il più vicino tra gli esempi positivi e negativi. Questo iperpiano è determinato solo da una piccola parte degli esempi di training, che vengono chiamati \emph{vettori di supporto}.

Le SVM si basano su una solida base teorica, e si comportano bene rispetto al problema dell'\textit{overfitting}. Inoltre non ci sono parametri esterni da impostare.

\subsection{Classifier Committees}

L'idea alla base dei comitati di classificatori è quella che generalmente un gruppo di esperti, combinando la loro conoscenza, può ottenere risultati migliori di un singolo esperto.

Nel metodo \textit{bagging} i singoli classificatori vengono appresi in parallelo dallo stesso \textit{training set}. Perchè questo approccio funzioni, i singoli classificatori devono essere significativamente diversi l'uno dall'altro (nella rappresentazione dei documenti, o nei metodi di apprendimento). I risultati dei singoli classificatori devono poi essere combinati. Il metodo più semplice è il voto a maggioranza, in cui una categoria è assegnata ad un documento se e solo se (k+1)/2 dei classificatori votano per quella categoria. Altra possibilità è una votazione pesata a maggioranza, dove la \textit{Categorization Status Value} è data dalla somma pesata delle CSV dei singoli classificatori. Il valore dei pesi può essere calcolato sulla base del comportamento dei classificatori in un \textit{validation set}.

Il \textit{boosting} è un altro metodo per combinare classificatori. Nel boosting i classificatori sono appresi in sequenza. Prima di apprendere l'i-esimo classificatore, i pesi dei singoli esempi nel \textit{training set} vengono ricalcolati dando maggior peso agli esempi che sono stati classificati scorrettamente dal classificatore precedente. AdaBoost è l'algoritmo più conosciuto tra quelli di \textit{boosting}.

\subsubsection{AdaBoost}

Sia $X$ lo spazio delle feature, $D=\{(d_1,c_1), (d_2,c_2), \ldots\}$ il training set, dove $d_i \in X$ sono le rappresentazioni dei documenti e $c_i \in \{ +1, -1\}$ l'assegnamento (binario) alla categoria. Un \textit{weak learner} è un algoritmo in grado di produrre una \textit{weak hypotesis} $h : X \rightarrow \{ \pm 1\}$ dato un insieme di esempi di training ed una distribuzione di pesi $W$ su di essi. La bontà di un'ipotesi è calcolata misurandone l'errore:

\[
\varepsilon(h,W) = \sum_{i:h(d_i) \neq c_i} W(i) 
\]

ossia la somma dei pesi degli esempi non classificati correttamente. \\ \\
Di seguito viene descritta la procedura adottata.
\begin{lstlisting}[frame=single, mathescape]
Inizializza la distribuzione dei pesi $W_1(i)= 1/|D|$ 
per ogni i

Ripeti per $t = 1 \ldots k$ 

Crea un weak classifier $h_t$ usando la distribuzione 
di pesi $W_t$

$\alpha_t = \frac{1}{2} \ln \frac{1- \varepsilon(h_t, W_t)}{\varepsilon(h_t, W_t)} $

Aggiorna i pesi

$
W_{t+1}(i) = Z_t \cdot W_t(i) \cdot 
	\left\{
	\begin{array}{l l} exp(-\alpha_t) & \quad \mathrm{if\ h_t(d_i) = c_i} \\
		exp(\alpha_t) &\quad \mathrm{otherwise}
	\end{array}	\right.
	
$
where $Z_i$ is the normalization factor.
\end{lstlisting}


Il classificatore finale si ottiene combinando i voti delle singole \textit{weak hypotesis} $H(d) = \mathrm{sign}(\sum_{t=1..k} \alpha_t h_t (d))$.

Si può dimostrare che se un weak learner genera classificatori che fanno meglio della classificazione casuale, l'errore del classificatore finale diminuisce esponenzialmente con il numero $k$ di passi. Inoltre, si può dimostrare una stretta correlazione tra AdaBoost e le SVM, poichè anch'esso massimizza il margine tra l'ipotesi e le istanze. AdaBoost ha in comune con le SVM anche la resistenza all'overfitting.


\subsubsection{Conclusioni}

\'E difficile asserire quale classificatore sia il migliore, ma si possono trarre alcune conclusioni generali:
\begin{itemize}
	\item I classificatori che ottengono le performance migliori sono SVM, AdaBoost, kNN e la regressione.
	\item Rocchio e Naive Bayes hanno le performance peggiori, ma vengono spesso usati per la semplicità di implementazione, o nei comitati di classificatori.
	\item Sono presenti risultati discordi riguardo le reti neurali e gli alberi di decisione: in alcuni contesti raggiungono performance paragonabili a quelle delle SVM, mentre in altri non si comportano in maniera soddisfacente.
\end{itemize}


