\chapter{Information Extraction}

\section{Introduzione}
L'\emph{Information Extraction} è una tecnica adottata nell'ambito del \emph{Natural Language Processing}, ed ha come obiettivo principale quello di riuscire ad estrarre informazioni strutturate dai documenti non strutturati a disposizione. In generale, i sistemi di IE sono utili se vengono soddisfatte queste condizioni:
\begin{itemize}
\item L'informazione che deve essere estratta dal testo è specificata a priori, e non è necessario fare inferenza.
\item Un numero esiguo di \emph{templates} è sufficiente per riassumere le parti principali del documento.
\item L'informazione di cui si ha bisogno è espressa in modo relativo e locale al testo in esame.
\end{itemize}
Innanzitutto ogni documento viene sottoposto a \emph{pre-processing}, al fine di riuscire a trovare \emph{entities} e \emph{relationships} che rappresentano il contenuto del testo, e che sembrano essere ricche di significato. In questo contesto, il termine \emph{relationships} viene usato per indicare fatti o eventi che coinvolgono certe entità.
Le informazioni estratte forniscono dei dati più concisi e precisi per il \emph{mining  process}, rispetto ad altri approcci naive \emph{word-based} (usati per esempio per la \emph{text categorization}), e l'informazione tende a rappresentare i concetti e le relazioni più significative e direttamente correlate con il dominio del documento analizzato.
IE può essere vista anche come una particolare forma di \emph{complete text comprehension}. In particolare vengono definiti a priori i tipi di informazione semantica che deve essere estratta dal documento. I documenti vengono rappresentati come insiemi di entità e \emph{frames}, un altro modo per descrivere formalmente le relazioni tra le entità. 

\subsection{Elementi estratti dal testo}
Esistono quattro tipi principali di elementi che possono essere estratti dal testo.
\begin{itemize}
\item \textbf{Entità:} rappresentano i blocchi principali che possono essere trovati in documenti.
\item \textbf{Attributi:} sono \emph{features} delle entità individuate.
\item \textbf{Fatti:} sono le relazioni tra le entità.
\item \textbf{Eventi:} sono attività o situazioni di interesse in cui sono coinvolte le entità.
\end{itemize}

\section{Evoluzione dell'IE}
Nelle sezioni successive vengono esposti i principali \emph{tasks} che sono stati identificati nel corso dell'evoluzione.

\subsection{\emph{Named Entity Recognition}}
La fase \emph{Named Entity Recognition} costituisce il \emph{task} principale di qualsiasi sistema di IE. Il sistema tenta di identificare tutti i nomi propri e le quantità all'interno del testo.
\begin{itemize}
\item Nomi di persona, locazioni geografiche, e organizzazioni;
\item Date e ore;
\item Quantità di denaro e percentuali.
\end{itemize}

\subsection{\emph{Template Element Task}}
\emph{Template Element Task} sono indipendenti dal dominio o dal contesto in cui ci si trova. Ogni TE è un oggetto generico e alcuni attributi lo possono descrivere. Questo consente di mantenere la separazione tra gli aspetti dominio-indipendenti e dominio-dipendenti dell'estrazione. Distinguiamo i seguenti tipi di TE:
\begin{itemize}
\item Persona
\item Organizzazione
\item Locazione
\end{itemize} 
Di seguito vengono forniti esempi di TE.\\ \\
\textit{Fletcher Maddox, primo preside dell'UCSD Business school, annunciò la costituzione de ``La Lolla Genomatics'' con i suoi due figli. La Lolla Genomatics rilascierà il suo primo prodotto \emph{Geninfo} nel giugno del 1999. L.J.G. ha avuto luogo nella città natia della famiglia Maddox, La Lolla in California.}\\ \\
Nel paragrafo precedente è possibile individuare le seguenti entità e descrittori attraverso un algoritmo di Information Extraction.
\begin{lstlisting}[frame=single, mathescape]
entity{
ID = 1
NAME = Fletcher Maddox 
DESCRIPTOR = Primo preside dell'USCD Business School 
CATEGORY = persona
}

entity{
ID = 2
NAME = La Lolla Genomatics
ALIAS = LJG
DESCRIPTOR = ''  
CATEGORY = organizzazione
}

entity{
ID = 3
NAME = La Lolla
DESCRIPTOR = il luogo natio della famiglia Maddox
CATEGORY = locazione
}
\end{lstlisting}

\subsection{\emph{Template Relationship Task}}
\emph{The Template relationship task} esprime una relazione dominio-indipendente tra entità, a differenza del TE che identifica semplicemente le singole entità. L'obiettivo del TR è quello di riuscire a trovare le relazioni che esistono tra gli elementi template estratti dal testo. Così come la definizione di una entità, anche gli attributi dell'entità dipendono dal problema e dalla natura dei testi analizzati. I seguenti TRs sono stati estratti dal paragrafo di esempio precedente.
\pagebreak
\begin{lstlisting}[frame=single, mathescape]
impiegato_di(Fletcher Maddox, USCD Business School)
impiegato_di(Fletcher Maddox, La Lolla Genomatics)
prodotto_di(Geninfo, La Lolla Genomatics)
locazione_di(La Lolla, La Lolla Genomatics)
locazione_di(California, La Lolla Genomatics)
\end{lstlisting}

\subsection{\emph{Scenario Template}}
Scenario templates (STs) esprime entità e relazioni all'interno di un certo dominio e inerenti ad un compito specifico. Lo scopo principale del ST è di testare la portabilità, analizzando la possibilità di trasferire il motore di estrazione in un nuovo dominio in tempi brevi. Prendendo il tsto di esempio è possibile individuare i seguenti eventi.

\begin{lstlisting}[frame=single, mathescape]
compagnia-formazione-evento {
PRINCIPALE = Fletcher Madddox
DATE = ''
CAPITAL = ''
}

prodotto-rilascio-evento {
COMPAGNIA = La Lolla Genomatics
PRODUCE = Geninfo
DATA = Giugno 1999
COST = ''
\end{lstlisting}

\subsection{\emph{Coreference Task}}
La \emph{Coreference Task} cattura informazioni riguardo espressioni \emph{coreferring}, ad esempio pronomi o altri menzioni dell'entità data, includendo quelle individuate nel NE e TE task. Il Co si focalizza sulla relazione di IDENTIT\`A, che è simmetrica e transitiva. Vengono create delle classi di equivalenza, dette anche ``catene di coreference'', usate per il punteggio. Il compito è quello di marcare nomi, predicati nominali e pronomi.  

\section{Architettura dei sistemi di IE}
La figura \ref{arch} mette in evidenza l'architettura generale di un sistema di \emph{Information Extraction}.
\begin{figure}[h]
\begin{center}
\includegraphics[width=120mm]{architettura.png}
\caption{Architettura di un tipico sistema di \emph{Information Extraction}}
\label{arch}
\end{center}
\end{figure}
\subsection{Flusso d'informazione in un sistema IE}
La maggior parte dei sistemi di IE usano un approccio deterministico bottom-up per analizzare il documento. Inizialmente, si identificano gli elementi a basso livello, e poi si individuano le features ad alto livello che sono basate su quelle a basso livello. Nell'elenco vengono esposte le fasi che è possibile individuare nel processo di \emph{Information Extraction}. 
\begin{itemize}

\item \textbf{Tokenization e analisi lessicale}\\ \\
In questa fase l'obiettivo è di processare il documento per individuare gli elementi base. Il testo è diviso in \emph{tokens}, frasi, e possibilmente paragrafi. Successivamente ogni parola viene etichettata con il proprio \emph{part of speech} e lemma.

\item \textbf{Identificazione dei predicati nominali}\\ \\
Dopo aver effettuato un'analisi di base, si procede identificando le diverse entità base, quali date, ore, indirizzi e-mail, organizzazioni, nomi di persona, locazioni e così via. Le entità sono individuate usando espressioni regolari che utilizzano il contesto in cui sono inseriti i nomi propri per identificare il loro tipo. Questa fase è condotta eseguendo lo scanning delle parole nella frase tentando di fare il match con uno dei pattern presenti nell'insieme di espressioni regolari predefinite.

\item \textbf{Parsing}\\ \\
A questo punto dopo aver individuato le entità base, si esegue il parsing ad alto livello e si identificano i nomi e verbi. Questi elementi sono usati nella fase successiva in cui si identificano le relazioni tra di essi.

\item \textbf{Costruzione relazioni} \\ \\
In questa fase per identificare le relazioni vengono utilizzati i pattern specifici del dominio. La generalità dei pattern dipende dalla profondità dell'analisi linguistica eseguita a livello delle frasi.

\item \textbf{Inferenza}\\ \\
In alcuni casi è necessario inferire i valori mancanti per completare l'identificazione degli eventi. Le regole d'inferenza sono scritte come un formalismo simile alle clausole Prolog. Esempi comuni includono relazioni familiari, cambiamenti direzionali, relazioni spaziali e così via. Di seguito viene proposto un esempio per mettere in evidenza l'uso delle regole d'inferenza.\\ \\
\emph{John Edgar vive con Nancy Leroy e il suo indirizzo è 101 Forest Rd., Bethlehem, PA}\\ \\
\`E possibile estrarre le seguenti entità ed eventi:
\begin{enumerate}
\item persona(John Edgar)
\item persona(Nancy Leroy)
\item vivereassieme(John Edgar,Nancy Leroy)
\item indirizzo(John Edgar, 101 Forest Rd., Bethlehem, PA)
\end{enumerate}
Usando la seguente regola si può inferire che Nancy Leroy vive al 101 Forest Rd., Bethlehem, PA.\\ \\
\emph{indirizzo(P1,A) :- persona(P1), persona(P2), vivereassieme(P1,P2), indirizzo(P1,A)}.

\end{itemize}