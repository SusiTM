\chapter{Introduzione}
\section{Il concetto di Knowledge Discovery}
Il Text mining si inserisce in un contesto molto ampio, che si identifica nel processo di scoperta della conoscenza, o anche noto con il termine di \emph{Knowledge Discovery}. La KD rappresenta un processo generico che prende in input una collezione di dati piuttosto consistente e che consente di estrarre pattern, ossia informazione legata ai dati stessi; questo paradigma comprende in realtà due processi più specifici che si distinguono per la tipologia di dati trattati.\\
Nelle sezioni successive verranno evidenziate le differenze tra i diversi approcci.

\subsection{Knowledge Discovery in Database}
KDD rappresenta il processo di scoperta di pattern a partire da dati contenuti in database e quindi strutturati; questi pattern devono essere:
\begin{itemize}
\item validi: estesi a nuovi dati con un certo grado di affidabilità
\item nuovi: sconosciuti almeno al sistema (o anche agli utenti)
\item potenzialmente utili: usati per prendere decisioni e quindi confermare o meno certe ipotesi
\item comprensibili: in termini di dimensioni, complessità, rappresentazione.
\end{itemize}
Secondo la formulazione di Fayayd, Piatetsky-Shapiro e Smyth, il processo di KDD può essere riassunto nei seguenti passi:
\begin{enumerate}
\item \emph{Understanding the application domain, the relevant prior knowledge, and the goals of the end-user}: conoscere il dominio applicativo, l'informazione a priori rilevante e gli obiettivi dell'utente finale.
\item \emph{Creating a target data set}: individuare il \emph{data set}, ossia il campione di dati che verrà trattato.
\item \emph{Data cleaning and preprocessing}: rimuovere inconsistenze e rumore dai dati, e scegliere le strategie per gestire i dati mancanti.
\item \emph{Data reduction and projection}: trovare elementi per caratterizzare la rappresentazione dei dati in funzione degli obiettivi.
\item \emph{Choosing the data mining task}: scegliere l'obiettivo da perseguire.
\item \emph{Choosing the data mining algorithm(s)}: stabilire gli algoritmi per individuare i pattern nei dati.
\item \emph{Data mining}: cercare i pattern interessanti sulla base degli obiettivi prefissati.
\item \emph{Interpreting mined patterns}: interpretare i risultati ottenuti.
\item \emph{Consolidating discovery knowledge}: integrare la conoscenza scoperta nei vari processi decisionali, o presentarla alle parti interessate.
\end{enumerate}  

\subsection{Knowledge Discovery in Text Database }
KDT consiste nel processo non banale di individuazione di pattern validi, nuovi, potenzialmente utili, e comprensibili in dati testuali. Il processo si contraddistingue sia per i dati che vengono trattati, ma anche per la tipologia dei pattern individuati. Caratteristica fondamentale dell'informazione scoperta è quella di essere "nuova"; infatti, i pattern individuati devono essere precedentemente sconosciuti anche agli autori dei testi presi in esame. 
Il KDT comprende due fasi distintive, TR e TM, che tratteremo in dettaglio di seguito.

\subsubsection{Text Refining}
La fase di Text Refining ha come scopo fondamentale quello di trasformare il testo ''grezzo'' di input che altrimenti non potrebbe essere sottoposto a \emph{mining}. Con questo procedimento infatti i dati assumono una forma sufficientemente strutturata e quindi analizzabile, preservando comunque le informazioni utili(\emph{intermediate form}). 
Il processo appena descritto può essere visto anche come un passaggio intermedio all'interno del KDT consistente nella preparazione dei dati da sottoporre all'analisi vera e propria.\\
Per assolvere questo compito vengono messe a disposizione varie tecniche:

\begin{itemize}
\item{\textbf{Tokenization}} \\
Rappresenta uno dei primi passi di analisi del linguaggio. Consiste nel suddividere il testo in \emph{token} ognuno dei quali corrisponde ad un'istanza di un tipo; di conseguenza il numero di \emph{token} sarà tanto più alto quanto maggiore è il numero dei tipi presenti. Per quanto riguarda il trattamento della punteggiatura, l'approccio dipende dalla necessità di mantenere o meno l'informazione relativa. In alcuni contesti si può decidere di considerare solo i confini di frase, trascurando la punteggiatura interna. Queste scelte dipendono chiaramente anche dall'ambiente in cui si opera.

\item{\textbf{Stemming}}\\
Dopo aver suddiviso il testo in \emph{tokens} è necessario effettuare la conversione in una forma standard. Lo \emph{stemming} rappresenta una forma possibile di standardizzazione in quanto si tratta di un processo che estrae la radice di una parola, eliminando affissi e desinenze. Questa operazione si limita però a troncare la parola mediante un'analisi morfologica semplificata che non sempre è corretta. L'effetto consiste nel ridurre il numero di tipi distinti presenti nel testo, e nell'aumentare la frequenza dell'occorrenza di alcuni tipi particolari.


\item{\textbf{Lemmatization}}\\
Costituisce un'altra forma di standardizzazione del testo. \'E un processo che cerca il lemma a partire da una parola con desinenza. Rispetta alla precedente tecnica, questa si occupa anche di disanbiguare tra le diverse forme base cui può corrispondere una forma flessa.

\item{\textbf{Finding Collocations}}\\
Una \emph{collocation} è una espressione dotata di significato, formata da due o più parole, che corrisponde a un qualche uso idiomatico. Importante è il fatto che il significato delle \emph{collocation} non può essere dedotto dal significato delle singole parti.

\item{\textbf{N-grams}}\\
Un \emph{n-gram} è una sequenza generica di \emph{n} parole. Si distinguono dalle \emph{collocations} in quanto non necessariamente essi corrispondono a un uso idiomatico(non sono per forza portatori di informazione semantica); molto spesso gli \emph{n-grams} rappresentano costruzioni sintattiche comuni. Sono utili nel caso in cui i testi trattati siano in lingue non note al sistema, o in ricerche che coinvolgano problemi di \emph{authorship analysis}.

\item{\textbf{Word Sense Disambiguation}}\\
Questa operazione consiste nel determinare quale dei significati di una parola ambigua si riferisce ad una occorrenza specifica. Ci possono essere infatti diversi tipi di ambiguità:
\begin{itemize}
\item Polisemia: i diversi significati attribuiti alla stessa parola sono legati etimologicamente e semanticamente
\item Omonimia: i diversi significati di un lessema si trovano a essere rappresentati da un'unica forma ortografica solo per caso
\end{itemize}
Esistono due classi di risoluzione del problema, una definita \emph{dictionary-based} che ricorre a risorse lessicali, e una basata su metodi di \emph{learning}.

\item{\textbf{Anaphora Resolution}}\\
L'anafora è una relazione tra una espressione linguistica, detta appunto anafora, e un'altra che la precede e ne determina il riferimento. Il metodo in esame ha il compito di identificare e risolvere le relazioni di anafora fra le entità all'interno di un testo; questo procedimento risulta comunque essere particolarmente difficoltoso, ed infatti i migliori processi possono garantire una precisione massima del 50-60\%.

\item{\textbf{Part of Speech Tagging}}\\
Consiste nell'attribuire ad ogni parola del testo un \emph{tag} con la sua corretta parte del discorso, determinando quindi se si tratta di un articolo, nome, verbo, aggettivo e così via. Il \emph{tagging} può essere visto anche come un'applicazione limitata di disambiguazione sintattica: nel caso in cui una parola appartenga a più categorie sintattiche, determina quale tra queste categorie è la più plausibile nel contesto dato. 
\end{itemize}


\subsubsection{Text Mining}
Il Text Mining rappresenta una fase specifica di selezione e applicazione di tecniche per la ricerca di pattern in dati derivati da testo. La scelta delle tecniche di TM è strettamente vincolata dalla natura della \emph{intermediate form} cui si applicano, e ovviamente al tipo di conoscenza che si vuole estrarre.\\ \\
Nei capitoli successivi andremo ad analizzare nel dettaglio le diverse tecniche a disposizione. La scelta del metodo da adottare dipende chiaramente dal tipo di conoscenza e informazione che si vuole estrarre dalla base documentale di cui si dispone. 





