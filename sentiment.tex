\chapter{Sentiment Analysis}

\section{Introduzione}

Il web contiene molte opinioni riguardo svariati argomenti (prodotti, personaggi famosi...) espresse in appositi siti. Di conseguenza, negli ultimi anni, il problema dell' \textit{opinion mining} ha suscitato un interesse sempre più crescente . Il materiale su cui si basa questo capitolo (\cite{NLP}) si focalizza sull'aspetto delle recensioni di \emph{prodotti}, come ad esempio quelle che si trovano su siti come \htmladdnormallink{amazon.com}{http://www.amazon.com}.
Questi siti spesso associano dei meta-dati ad ogni recensione, che indicano quanto questa sia positiva o negativa, usando ad esempio una scala a 5 stelle. Il problema è che l'opinione del lettore può essere diversa da quella di colui che ha scritto la recensione. Ad esempio, il giudizio del lettore potrebbe essere particolarmente influenzato dalla qualità della palestra in un hotel, mentre i recensori potrebbero considerare importanti altri aspetti, come la locazione o l'arredamento. In uno scenario simile, il lettore è costretto a leggere molte recensioni alla ricerca delle informazioni riguardo le caratteristiche che gli interessano.

Il problema del \textit{review mining} può essere scomposto in 4 sottoproblemi:
\begin{itemize}
	\item \textbf{Identificare le caratteristiche (features) del prodotto}: possono essere esplicite (es. `` le dimensioni sono troppo grandi'') o implicite (es. ``lo scanner è lento'', un riferimento alla velocità dello scanner).
	\item \textbf{Identificare le opinioni riguardo le specifiche features}: ad esempio, la frase ``le dimensioni sono troppo grandi'' contiene l'opinione ``troppo grandi'' riguardo la \textit{feature} ``dimensioni''.
	\item \textbf{Determinare la \textit{polarità} delle opinioni}: cioè capire se le opinioni sono positive o negative.
	\item \textbf{Ordinare le opinioni} in base alla loro intensità: ad esempio ``orribile'' è un espressione più forte di ``brutto''.
\end{itemize}

\section{OPINE}
Vedremo il problema della \textit{Sentiment Analysis} osservando il sistema OPINE, un sistema di \textit{Information Extraction} non supervisionato che propone una soluzione per i problemi appena citati.
Dato un prodotto e un insieme di recensioni, OPINE da in \textit{output} una lista di \textit{features} del prodotto, ognuna delle quali ha associata una lista di opinioni, ordinate sulla base della loro intensità.
\subsection{Terminologia}
Una \emph{classe di prodotti} (es. Scanner) è un insieme di prodotti (es. Epson1200). OPINE estrae caratteristiche appartenenti alle seguenti classi:
\begin{itemize}
	\item proprietà (es. Dimensione dello scanner)
	\item componenti (es. Cover dello scanner)
	\item features delle componenti (es. Durata della batteria)
	\item concetti correlati (es. Immagine scannerizzata)
	\item componenti e proprietà dei concetti correlati (es. Dimensione dell'immagine scannerizzata)
\end{itemize}

\subsection{Overview}
Data la classe di prodotti \textit{C} con istanze \textit{I} e le corrispondenti recensioni \textit{R}, OPINE trova un insieme di tuple (feature, opinioni) $\{ (f,o_i, \ldots, o_j) \}$ tale che $f \in F$ e $o_i, \ldots, o_j \in O$, dove $F$ è l'insieme delle features contenute in $R$ e $O$ è l'insieme di opinioni in $R$. $f$ è una feature di un particolare prodotto, $o$ è un opinione riguardo a $f$ in una particolare frase, e le opinioni associate a $f$ sono ordinate per intensità.

I passi principali di OPINE possono essere riassunti nella seguente procedura.

\begin{lstlisting}[frame=single, mathescape]
Input: classe di prodotti C, recensioni R
Output: lista di [feature, lista di opinioni ordinata]
R' $\leftarrow$ parseReviews(R);
E  $\leftarrow$ findExplicitFeatures(R', C);
O  $\leftarrow$ findOpinions(R', E);
CO $\leftarrow$ clusterOpinions (O);
I  $\leftarrow$ findImplicitFeatures(CO, E);
RO $\leftarrow$ rankOpinions(CO); 
{$(f, o_i, \ldots, o_j)$} $\leftarrow$ outputTuples(RO, I $\cup$ E)

\end{lstlisting}

In seguito vedremo più in dettaglio ogni passaggio.

\subsection{Trovare features esplicite}
OPINE estrae le features esplicite per una data classe di prodotti a partire dal parsing delle recensioni. Il sistema identifica ricorsivamente le componenti e le proprietà per la classe finchè non vengono più identificate features. Successivamente vengono ricercati i concetti correlati, e ne estrae componenti e proprietà. 
Per fare questo, il sistema prima individua i predicati nominali dalle recensioni, e considera solo quelli con una frequenza più alta di una certa soglia definita sperimentalmente. 
Questi predicati sono passati al \textit{feature assessor}, che valuta ogni predicato nominale calcolando un valore chiamato PMI  tra il predicato e i discriminatori di meronimia (es., “dello scanner”, “ scanner ha”, “scanner arriva con”, per la classe di prodotti ``Scanner''). OPINE distingue le parti dalle proprietà utilizzando la gerarchia \textit{IS-A} di \textit{WordNet}, e informazioni morfologiche (es. per l'inglese i suffissi ``-iness'' o ``-ity'').
Per trovare i concetti correlati OPINE analizza i verbi che collegano i vari predicati nominali.

\subsection{Trovare features implicite}
Il sistema inizialmente estrae le espressioni di opinione relative alle features esplicite (come vedremo in seguito). Le opinioni esplicite si riferiscono a proprietà (es. ``pulito'' si riferisce alla proprietà ``pulizia''). OPINE esamina le espressioni di opinione associate alle features esplicite per individuare quelle implicite. Il problema è associare aggettivi alle relative proprietà. Per fare questo si adotta WordNet che consente di creare dei cluster di aggettivi, successivamente etichettati con il nome della proprietà a cui si riferiscono.

\subsection{Individuare espressioni di opinione e la loro polarità}

Per applicare questa tecnica si deve tener presente l'idea di base secondo cui le opinioni relative ad una caratteristica di un prodotto, compaiano vicine ad essa all'interno del testo. Anzichè utilizzare una finestra di dimensione k fissata, si usano le dipendenze individuate dal parser, e delle regole di estrazione che sono riportate in tabella \ref{fig:Extraction_rules}. Quando una feature esplicita è individuata, OPINE applica le regole al fine di trovare espressioni di opinione. La notazione che viene adottata d'ora in poi è la seguente:
\begin{itemize}
\item po = potenziale opinione
\item M = modificatore
\item NP = predicato nominale
\item S = soggetto
\item P = predicato
\item O = oggetto
\end{itemize}
Le opinioni espresse vengono indicate tra parentesi.
\begin{figure}[ht]

\begin{tabular}{|l|l|}
	\hline
	\textbf{Extraction Rules} & \textbf{Examples} \\
	\hline
	if $\exists (M, NP = f) \leftarrow po = M$ & (expensive) scanner \\
	if $\exists (S = f, P, O) \leftarrow po = O$ & lamp has (problems) \\
	if $\exists (S, P, O = f) \leftarrow po = P$ & I (hate) this scanner \\
	if $\exists (S = f, P) \leftarrow po = P$ & program (crashed) \\
	\hline
\end{tabular}
\caption{Regole per estrarre potenziali espressioni di opinione}
\label{fig:Extraction_rules}
\end{figure}

Successivamente il sistema esamina le potenziali opinioni individuate, trovandone l'orientazione semantica e distinguendo tra opinioni positive o negative.

\subsubsection{Context-specific word semantic orientation}

Dato un insieme di etichette di orientazione semantica $SO =\{ positive, negative, neutral\}$, un insieme di recensioni e un insieme di tuple $(w, f, s)$, dove $w$ è la potenziale opinione, $f$ la feature associata ed $s$ la frase in cui compaiono, il sistema assegna una etichetta $SO$ ad ogni tupla usando tecniche di classificazione non supervisionata sulla base di vincoli locali (tecniche di computer-vision): \emph{relaxional labeling}.

\subsubsection{Relaxional labeling}

\'E una tecnica di classificazione non supervisionata che prende in input:
\begin{itemize}
	\item un insieme di oggetti (in questo caso parole)
	\item un insieme di etichette ( le $SO$ label)
	\item distribuzione iniziale di probabilità per ogni etichetta possibile
	\item definizione di vicinato di un oggetto
	\item definizione delle features del vicinato
	\item definizione di una funzione di supporto per un etichetta ad un oggetto
\end{itemize}
 La \emph{funzione di supporto} calcola la probabilità dell'assegnamento dell'etichetta L all'oggetto o sulla base delle feature dei suoi vicini. \'E una procedura iterativa, di tipo \textit{Expectation Maximization}, che raggiunge la convergenza.\\ \\
I vincoli locali su cui si basa OPINE sono:
\begin{itemize}
	\item congiunzioni e disgiunzioni nel testo
	\item template di regole di dipendenza sintattica
	\item relazioni morfologiche tra oggetti
	\item sinonimie, antinomie e relazioni morfologiche fornite da WordNet
\end{itemize}

\subsection{Ordinare le opinioni}
Per ogni feature, le opinioni sono ordinate sulla base della loro \textit{intensità}. Le probabilità generate al passo precedente sono l'assegnamento iniziale, che poi viene modificato introducendo altri vincoli di tipo probabilistico, riducendo il problema a risolvere dei CSP probabilistici.

\subsection{Prestazioni}
Per misurare l'accuracy del sistema, è stato assegnato manualmente un punteggio alle opinioni riguardo le 30 più frequenti \textit{features} di un insieme di recensioni: se due opinioni consecutive nel ranking sono in ordine diverso rispetto al giudizio umano, si considera un errore. L'accuracy del sistema è risultata essere del 73\%.
